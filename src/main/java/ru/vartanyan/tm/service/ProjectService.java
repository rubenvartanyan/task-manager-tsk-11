package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IProjectRepository;
import ru.vartanyan.tm.api.IProjectService;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findObeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project remove(Project project) {
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        Project project = projectRepository.findOneById(id);
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        Project project = projectRepository.findOneByIndex(index);
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = projectRepository.findOneByName(name);
        return projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project add(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description){
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description){
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
