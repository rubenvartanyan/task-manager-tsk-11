package ru.vartanyan.tm.api;

public interface IProjectController {

    void showList();

    void showProjectByIndex();

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

    void create();

    void clear();

}
