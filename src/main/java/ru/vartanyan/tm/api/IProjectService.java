package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findObeByName(String name);

    Project add(Project project);

    Project remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    void clear();

    Project add(String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);
}
