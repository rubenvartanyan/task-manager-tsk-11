package ru.vartanyan.tm.api;

public interface ITaskController {

    void showList();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void create();

    void clear();

}
